<?php
/**
 * Created by PhpStorm.
 * User: ozak
 * Date: 20.02.17
 * Time: 16:49
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class chatType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author','hidden',array(
            'data' => $options['name']
        ))
            ->add('msg','text',array(
            'label' => false
    ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\message',
            'attr' => ['id' => 'chat-form']
        ));
        $resolver->setRequired(array(
            'name'
        ));
    }
}