<?php

namespace AppBundle\Controller;


use AppBundle\Entity\message;
use AppBundle\Entity\visitor;
use AppBundle\Form\chatType;
use AppBundle\Form\visitorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/userload", name="userload")
     */
    public function userLoadAction(Request $request)
    {
        $sendLog=$this->getChatLog('all', $request->getSession()->get('visitor')); // wszystkie wiadomości uczestnika
        return $this->render('element/user-log.html.twig', array('sendLog'=>$sendLog));
    }
    /**
     * @Route("/chatload", name="chatload")
     */
    public function loadAction(Request $request)
    {
        $chatLog=$this->getChatLog();
        return $this->render('element/messages.html.twig', array('chatLog'=>$chatLog));
    }
    /**
     * @Route("/sentload", name="sentload")
     */
    public function sentListAction(Request $request)
    {
        $chatLog=$this->getChatLog(message::SENT_MSG,'','ASC');
        return $this->render('element/judge.html.twig', array('waitLog'=>$chatLog));
//        return $this->render('default/empty.html.twig', array('waitLog'=>$chatLog));

    }
    /**
     * @Route("/justicehammer", name="justicehammer")
     */
    public function judgeAction(Request $request)
    {
        if(($request->request->get('ok')!=null) or ($request->request->get('no')!=null)){
            $this->changeStatus($request);
            return $this->render('default/empty.html.twig');
        }
    }
    /**
     * @Route("/{string}", name="homepage", defaults={"string": ""})
     */
    public function indexAction(Request $request, $string)
    {
//        $request->getSession()->set('visitor',null);
//        $request->getSession()->set('moderator',null);
//        $request->getSession()->set('ekspert',null);
//        $request->getSession()->set('stage',null);

//ZANOTOWANIE, ŻE LOGOWAĆ BĘDZIE SIĘ MODERATOR LUB EKSPERT
        if($string === 'key1key1key1key1key1key1key1key1key1key1')
        {$request->getSession()->set('moderator', 'logged');}
        if($string === 'key2key2key2key2key2key2key2key2key2key2')
        {$request->getSession()->set('ekspert', 'logged');}

/*        var_dump( ($request->getSession()->get('moderator') != null) and ($request->getSession()->get('visitor') != null) );
        var_dump( $request->getSession()->get('moderator') );*/
        if( $request->request->get('visitor')!= null  ) {
            $request->getSession()->set('visitor', $request->request->get('visitor'));
        }

/*        var_dump($request->request->get('visitor'));
        var_dump($request->getSession()->get('visitor'));*/
//ZANOTOWANIE NAZWY UCZESTNIKA test commitowania po rebasie z pushem +master
        $name= ($request->getSession()->get('visitor') != null ) ? $request->getSession()->get('visitor') : 'anonim';

        $visitor = new visitor();
        $visitor_form = $this->createForm(visitorType::class, $visitor);
        $visitor_form->handleRequest($request);
//ODNOTOWANIE NAZWY MODERATORA LUB EKSPERTA
        if (is_array($request->getSession()->get('visitor')))
        {
            $name = $visitor_form->getData()->getName();
            $request->getSession()->set('visitor',$name);

        }

        $message = new message();
        $message_form = $this->createForm(chatType::class, $message,array('name' => $name));
        $message_form->handleRequest($request);


// REQUEST MODERATORA LUB EKSPERTA
        if($visitor_form->isSubmitted() && $visitor_form->isValid()){
            $visitors = $this->getDoctrine()->getRepository('AppBundle:visitor')->findAll();
                foreach($visitors as $visitor){
                    if($visitor->getName() == $visitor_form->getData()->getName()){
                        $this->get('session')->getFlashBag()->set('error', 'Already taken');
                        return $this->redirectToRoute('homepage',array('string'=>''),303);
                    }
                }
            $em = $this->getEntityManager();
            $visitor->setName($visitor_form->getData()->getName());
            $em->persist($visitor);
            $em->flush();
            $request->getSession()->set('stage','logged');
            $stage = 'logged';
            return $this->redirectToRoute('homepage',array('string'=>''),303);

            }else{
            $stage = '';
        }
// REQUEST NOWEGO UCZESTNIKA
        if($request->getSession()->get('visitor') != null and $request->request->get('entered')!=null) {

            $em = $this->getEntityManager();
                $visitor->setName($request->getSession()->get('visitor'));
            $em->persist($visitor);
            $em->flush();

        }
// PRZESYŁANIE WIADOMOŚCI DO AKCEPTACJI
        if( $request->request->get('msg')!=null){

            $em=$this->getEntityManager();
                $message->setAuthor($name);
                $message->setMsg($request->request->get('msg'));
            if( $request->getSession()->get('moderator')!=null or $request->getSession()->get('ekspert')!=null )
                $message->setStatus(message::GOOD_MSG);
            else $message->setStatus(message::SENT_MSG);
            $em->persist($message);
            $em->flush();
        }
        //        if($request->getSession()->get('moderator') != null or $request->getSession()->get('ekspert') != null)

        $chatLog = $this->getChatLog(); // message::GOOD_MSG


/*        if(($request->getSession()->get('moderator') != null) and ($request->getSession()->get('visitor') != null))
            $waitLog = $this->getChatLog(message::SENT_MSG);*/



        if(($request->getSession()->get('moderator') != null) and ($request->getSession()->get('visitor') == null))
            return $this->render('default/expert.html.twig', array(
                'message_form' => $message_form->createView(),'visitor_form' => $visitor_form->createView()
            ));
        else if(($request->getSession()->get('moderator') != null) and ($request->getSession()->get('visitor') != null))
            return $this->render('default/expert.html.twig', array(
                'message_form' => $message_form->createView(),'visitor_form' => $visitor_form->createView()
            ));
        else if($request->getSession()->get('ekspert') != null) // panel moda...
            return $this->render('default/expert.html.twig', array(
                'message_form' => $message_form->createView(),'visitor_form' => $visitor_form->createView()
            ));
        else
         return $this->render('default/home.html.twig', array(
            'message_form' => $message_form->createView(),'visitor_form' => $visitor_form->createView()
        ));
    }
    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    public function changeStatus(Request $request){

        if($request->request->get('ok') != null){
            $message = $this->getMessageRepository()->find($request->request->get('ok'));
            $message->setStatus(message::GOOD_MSG);
        }else if($request->request->get('no') != null){
            $message = $this->getMessageRepository()->find($request->request->get('no'));
            $message->setStatus(message::BAD_MSG);
        }
        $em = $this->getEntityManager();
        $em->flush();

    }

    protected function getChatLog($status = message::GOOD_MSG,$author = '',$order = 'DESC')
    {
        if($author!=''){
            if($status == 'all'){
                return $this->getMessageRepository()->findBy(['author'=>$author], array('date' =>$order));
            }else{
                return $this->getMessageRepository()->findBy(['status'=>$status,'author'=>$author], array('date' =>$order));
            }

        }
        return $this->getMessageRepository()->findBy(['status'=>$status], array('date' =>$order));
    }

/* /*   protected function getSentLog($status = message::SENT_MSG)
    {
        if($author!=''){
            return $this->getMessageRepository()->findBy(['status'=>$status], array('id' =>'ASC'));

        }
        return $this->getMessageRepository()->findBy(['status'=>$status], array('id' =>'DESC'));
    }*/
    protected function getMessageRepository(){
        return $this->getDoctrine()->getRepository('AppBundle:message');
    }

}
